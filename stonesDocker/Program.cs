﻿using System;

namespace stonesDocker
{
    class Program
    {
      static void Main(string[] args)
        {
            while(true){
            Console.WriteLine("\nChoose an option:");
            Console.WriteLine("1) Stones");
            Console.WriteLine("2) Hulk");
            Console.WriteLine("3) Watermelon");
            Console.WriteLine("4) Chips");
            Console.WriteLine("5) Sugar");
            Console.WriteLine("6) Exit");
            Console.Write("\r\nSelect an option: ");
 
            switch (Console.ReadLine())
            {
                case "1":
                    Stones();
                    break;
                case "2":
                    Hulk();
                    break;
                case "3":
                    watermelon();
                    break; 
                case "4":
                    chips();
                    break;   
                case "5":
                    Environment.Exit(0);
                    break;
               
            }
            }
        }
        
 
        public static void Stones(){
            Console.Clear();
            Console.WriteLine("Stones");

            int n = Convert.ToInt32(Console.ReadLine());
            string s = Console.ReadLine();

            int count = 0;
            char c = s[0];

            for(int i = 1; i < s.Length; i++){
                if(c == s[i]){
                    count ++;
                }
                c = s[i];
            }
            Console.WriteLine("ans: " + count);
        }

        private static void Hulk(){
            Console.Clear();
            Console.WriteLine("Hulk");

           int n=Convert.ToInt32(Console.ReadLine());
           for (int i=1;i<=n;i++){
            if (i%2==1)
                Console.Write("I hate bryan again");
            else
                Console.Write("I love bryan benchmark");
            if (i==n)
                Console.WriteLine("it");
            else
                Console.Write("that ");
            }
        }

        static void watermelon(){

        int number = Convert.ToInt32(Console.ReadLine());
        if(number%2 == 0 && number>2) {   //determines whether the inputted number can divide the watermelon equally
            Console.WriteLine("YES"); 
        } else {
            Console.WriteLine("NO");
        }

        }

        static void chips() {
            string input= Console.ReadLine();
            string[] num = input.Split(' ');
            int n = int.Parse(num[0]);
            int m = int.Parse(num[1]);
            int x = 1;
            while( m >= x ){
                m -= x;

                if (x == n) {
                    x = 1;
                } else {
                    x++;
                }
            }
            Console.WriteLine(m);
        }
 
    }
}
